/*
  @author: Nabson Paiva
  @subject: Organização de Computadores 2019/1 - CB01
  @description: Soma de números floats
*/

#include <stdio.h>
#include<stdlib.h>

#define sig 0x007FFFFF

typedef struct i3e {
  unsigned sinal;
  unsigned expo;
  unsigned mantissa;
  unsigned *valor;
} i3e;

int main() {
  float a, b, c = 0;
  char op;
  i3e x, y, r;
  scanf("%f %c %f", &a, &op, &b);

  // Cria a struct com os valores do padrão I3E separadamente para cada num.
  x.sinal = 0;
  x.valor = (unsigned *) &a;
  x.expo = (*(x.valor) >> 23) & 0xFF;
  x.mantissa = (*(x.valor) & sig) + 0x00800000;

  y.sinal = 0;
  y.valor = (unsigned *) &b;
  y.expo = (*(y.valor) >> 23) & 0xFF;
  y.mantissa = (*(y.valor) & sig) + 0x00800000;

  r.sinal = 0;
  r.valor = (unsigned *) &c;

  // Verifica se algum deles é 0
  if (a == 0) {
    printf("Resultado: %f\n", b);
    return 0;
  } else {
    if (b == 0) {
      printf("Resultado: %f\n", a);
      return 0;
    } else {
      // Expoentes são diferentes
      if (x.expo != y.expo) {
        while (x.expo > y.expo) {
          y.expo++;
          y.mantissa = y.mantissa >> 1;
          if (y.mantissa == 0) {
            printf("ERRO: Mantissa do b zerou.\n");
            return 0;
          }
        }
        while (x.expo < y.expo) {
          x.expo++;
          x.mantissa = x.mantissa >> 1;
          if (x.mantissa == 0) {
            printf("ERRO: Mantissa do a zerou.\n");
            return 0;
          }
        }
      }
      r.mantissa = x.mantissa + y.mantissa;
      r.expo = x.expo;
      // Overflow no Significando
      if ( (r.mantissa & 0xff000000) != 0 ){
        r.mantissa = r.mantissa >> 1;
        r.expo = r.expo + 1;
        // Overflow de expoente
        if ((r.expo & 0xffffff00) != 0) {
          printf("ERRO: Overflow de expoente.");
          return 0;
        }
      }
      // Normaliza o resultado cortando o leading 1
      r.mantissa = r.mantissa & 0x007fffff;
      *(r.valor) = (r.sinal << 31) + (r.expo << 23) + r.mantissa;
      printf("Resultado: %f\n", c);
      }
    }
  return 0;
}
