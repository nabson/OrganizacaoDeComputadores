/*
  @author: Nabson Paiva
  @schoolSubject: OC - Ciência da Computação
*/

#include <stdio.h>

int main () {
  if (0 == (unsigned) 0) {
    printf("unsigned 1\n");
  } else {
    printf("unsigned 0\n");
  }
  if (-1 < 0) {
    printf("signed   1\n");
  } else {
    printf("signed   0\n");
  }
  if ( -1 < (unsigned) 0){
    printf("unsigned 1\n");
  } else {
    printf("unsigned 0\n");
  }
  if ( 2147483647 > -2147483647 - 1 ){
    printf("signed   1\n");
  } else {
    printf("signed   0\n");
  }
  if ( (unsigned) 2147483647 > -2147483647 - 1 ){
    printf("unsigned 1\n");
  } else {
    printf("unsigned 0\n");
  }
  if ( 2147483647 > (int) (unsigned) -2147483647 - 1 ){
    printf("signed   1\n");
  } else {
    printf("signed   0\n");
  }

  if (-1 > -2){
    printf("signed   1\n");
  } else {
    printf("signed   0\n");
  }
  if ( (unsigned) -1 > -2 ){
    printf("unsigned 1\n");
  } else {
    printf("unsigned 0\n");
  }
  return 0;
}
